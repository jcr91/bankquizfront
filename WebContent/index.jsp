<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Home Banking</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="js/main.js"></script>
</head>
<body>





<div class="container-fluid">
      <div class="col-12 col-md-3 col-xl-2 bd-sidebar">
</div><div class="row flex-xl-nowrap">
          <main class="col-12 col-md-9 col-xl-8 py-md-3 pl-md-5 bd-content" role="main"><div class="bd-example">
          
          <H1>Home Banking</H1>
<br>
<form>
  <div class="form-row">
    <div class="form-group col-md-6">
      <label for="inputEmail4">Moneda de Orgien</label>
      <select disabled id="inputState" class="form-control">
        <option selected="">USD</option>
        <option>...</option>
      </select>
    </div>
    <div class="form-group col-md-6">
      <label for="inputPassword4">Moneda de Destino</label>
        <select  id="cboDestino" name="cboDestino" class="form-control">
        <option value="">--Seleccione--</option>
      </select>
    </div>
  </div>
  
   <div class="form-row">
    <div class="form-group col-md-6">
      <label for="inputEmail4">Tipo de Cambio</label>
      <input readonly type="text" class="form-control"  id="lblTipoCambio" name="lblTipoCambio" placeholder="0.00">
    </div>
    <div class="form-group col-md-6">
      <label for="inputPassword4">Monto a cambiar</label>
      <input  type="text" class="form-control"  id="txtMonto" name="txtMonto" placeholder="0.00">
    </div>
  </div>
  
  <button type="button" id="btnCalcular" name="btnCalcular"  class="btn btn-primary" disabled>Calcular</button>
  
  <br>
  <div class="form-group ">
    <label for="inputAddress">Monto Final</label>
    <input readonly type="text" class="form-control"  id="lblResultado" name="lblResultado" placeholder="0.00">
  </div>
  
  
</form>

<br><br>

<form>
<button type="button" id="btnAtm" name="btnAtm"  class="btn btn-primary" disabled>Buscar ATM</button>
<br><br>
<div class="w3-padding w3-white notranslate">
<div class="table-responsive">
<table class="table table-striped">
<thead>
<tr>
<th rowspan="2">Direction</th>
<th rowspan="2">Functionality</th>
<th rowspan="2">Type</th>
</tr>
</thead>
<tbody id="tbodyATM" name="tbodyATM">

</tbody>
</table>
</div>
</div>

</form>

</div>
    </main></div></div>




</body>


</html>