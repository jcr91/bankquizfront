var tipoCambio;
var valorCambio;
var token;
var valorCambio;

jQuery(document).ready(function() {
	
	/*Elemento que cambia el tipo de moneda*/
	$(document).on("change","#cboDestino",function(){ 
		 valorCambio = busquedaTipoCambio($(this).val());
		$("#lblTipoCambio").val(valorCambio);
	})
	
	/*Elemento que realiza el calco del tipo de cambio por el valor ingresado*/
	$(document).on("click","#btnCalcular",function(){ 
		var monto =$("#txtMonto").val()
		var valor =  parseFloat(monto) * parseFloat(valorCambio);
		saveChange();
		$("#lblResultado").val(valor);
	})
	
	/*Elemento para listar todos los ATM*/
	$(document).on("click","#btnAtm",function(){ 
		getATM()
	})
	
	
});

/*Funcion que registra el calculo del tipo de cambio seleccionado por el cliente*/
function saveChange(){
	var fromMon =  $( "#cboDestino option:selected" ).text();	
	var monto = $("#txtMonto").val();
	var settings = {
			  "url": "http://srpintbootapibank-env.eba-vq9sjyny.us-east-2.elasticbeanstalk.com/change/save",
			  "method": "POST",
			  "timeout": 0,
			  "headers": {
			    "Content-Type": "application/json",
			    "Authorization": "Bearer "+ token
			  },
			  "data": JSON.stringify({
			    "idClient": "inteligo",
			    "fromMoney": "USD",
			    "toMoney": fromMon,
			    "exchangeRate": monto,
			    "money": valorCambio
			  }),
			};

			$.ajax(settings).done(function (response) {
				if(response.code != 200){
					alert("Estimado cliente, ocurrio un problema intente nuevamente por favor.")
				}
			});
			
}

/*Funcion que obtiene las monedas y los tipo de cambio disponible para armar el campo de seleccion de moneda de destino*/
function getToMoneda(){
	
	var settings = {
			  "url": "http://srpintbootapibank-env.eba-vq9sjyny.us-east-2.elasticbeanstalk.com/exchangerate/all",
			  "method": "GET",
			  "timeout": 0,
			  "headers": {
			    "Content-Type": "application/json",
			    "Authorization": "Bearer "+ token
			  },
			};

			$.ajax(settings).done(function (response) {
				
				tipoCambio = response.data.quotes;
				var strCBO = "";
				$("#cboDestino").html("");
				strCBO += "<option value=''>--Seleccione--</option>";
				$.each(tipoCambio, function(indx, val){
					strCBO += "<option value="+indx+">"+indx.substring(3, 6);+"</option>";										
				})
				$("#cboDestino").html(strCBO);
			});
		
}

/*Funcion que obtiene el tipo de cambio de una moneda en especifico.*/
function busquedaTipoCambio(moneda){
	var tipCam = 0;

	$.each(tipoCambio, function(indx, val){
		if(moneda==indx){
			tipCam = val;
		}
	});
	valorCambio = tipCam;
	return tipCam
}

/*Funcion que obtiene el listado de los ATM.*/
function getATM(){
	$("#tbodyATM").html("");
	$("#tbodyATM").html("<tr> <td> Cargando. . . </td> </tr> ");
	var settingsATM = {
			  "url": "http://srpintbootapibank-env.eba-vq9sjyny.us-east-2.elasticbeanstalk.com/atm/getATM",
			  "method": "GET",
			  "timeout": 0,
			  "headers": {
			    "Content-Type": "application/json",
			    "Authorization": "Bearer "+ token
			  },
			};

			$.ajax(settingsATM).done(function (response) {
				var atm = response.data.atmbean;			
					var strBody = "";
					$("#tbodyATM").html("");
					
					$.each(atm, function(indx, val){
						strBody += "<tr> ";
						strBody += "<td>"+val.address.street + " " + val.address.housenumber + " - " + val.address.city+ "</td> ";
						strBody += "<td>"+val.functionality+ "</td> ";
						strBody += "<td>"+val.type+ "</td> ";
						strBody += "</tr>"					
						
					})
					
					$("#tbodyATM").html(strBody);
			});
	
	
}

/*Funcion inicial que obtiene el token de seguridad para interactuar con las apis.*/
var settingsLogin = {
		  "url": "http://srpintbootapibank-env.eba-vq9sjyny.us-east-2.elasticbeanstalk.com/auth/login",
		  "method": "POST",
		  "timeout": 0,
		  "headers": {
		    "Content-Type": "application/json",
		    "Authorization": "Basic Og=="
		  },
		  "data": JSON.stringify({
			  "user": "userbank",
			  "key": "quiz12345"
			  }),
		};

		$.ajax(settingsLogin).done(function (response) {
		  if(response.code == 200){
			  token = response.data.jwt;  
			  getToMoneda()
			  $("#btnCalcular").removeAttr('disabled');
			  $("#btnAtm").removeAttr('disabled');
		  }
		  
		});